<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\Cart;
use App\Product;
use App\Order;
use Session;

class CartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Cart::where('user_id', Auth::id())->get();
        $price = Cart::where('user_id', Auth::id())->sum('subtotal');

        return view('cart', compact('items', 'price'));
    }

    public function checkout()
    {
        $items = Cart::where('user_id', Auth::id())->get();
        $price = Cart::where('user_id', Auth::id())->sum('subtotal');
        $user = Auth::user();
        if ($items->count() == 0)
        {
            Session::flash('error', 'Cart cannot null! order something in shop...');
            return back();
        }
        else
        {
            return view('checkout', compact('items', 'price', 'user'));
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Cek di tabel apakah ada produk pilihan user yang sama atau tidak
        $find = Cart::where([
                        ['user_id', Auth::id()],
                        ['product_id', $request->product_id],
                    ])->count();
        // Ambil row data dari tabel
        $cart = Cart::where([
                        ['user_id', Auth::id()],
                        ['product_id', $request->product_id],
                    ])->first();
        if ($find == 1)
        {
            $quantity = $cart->quantity;
            $cart->quantity = $quantity + 1;
            $cart->subtotal = $cart->subtotal * $cart->quantity;
            $cart->save();

            // Session::flash('success', 'Item successfully added to cart!');
        }else
        {
            $newItem = new Cart;

            $newItem->user_id = Auth::id();
            $newItem->product_id = $request->product_id;
            $newItem->quantity = $request->quantity;
            $newItem->subtotal = $request->subtotal;
            $newItem->save();

            // Session::flash('success', 'Item successfully added to cart!'); 
        }
        
        return response()->json([
            'status' => 1,
            'message' => 'Item successfully added to cart'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::find($id)->delete();

        Session::flash('delete', 'Item has been deleted!');

        return redirect('/cart');
    }
}
